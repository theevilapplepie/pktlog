##
#   PacketLog Makefile
#   (C) James Vess
##

### Make Configuration

 # Name of Application Binary
 APPBIN=pktlog

 # Location directory for object files
 OBJLOC=objects

 # Location directory for source files
 SRCLOC=src

 # Location directory for header files
 HEADERLOC=$(SRCLOC)/headers

 # Location directory for resource files
 RESLOC=resources

 ## Source to Compile and Link ( without extension ) -- This will probably be automated later
 SRC_FILES=pktlog pktlog_cli pktlog_genutils pktlog_http pktlog_netutils pktlog_pcap pktlog_sqlite pktlog_html
 RESOURCE_FILES=computerguy.jpg

 ## gcc Configuration
 CC=gcc
 CFLAGS=-Wall -O2 -I $(HEADERLOC)
 LINK_FLAGS=-lpcap
 
 ## objdump Configuration
 RESOURCEOBJ_TYPE_32=elf32-i386
 RESOURCEOBJ_TYPE_64=elf64-x86-64

### END Configuration -- DO NOT TOUCH BELOW ###

## Internal Variables
_SRCOBJS = $(addprefix $(OBJLOC)/, $(addsuffix .o,$(SRC_FILES)))
_RESFILES = $(addprefix $(RESLOC)/, $(RESOURCE_FILES))
_RESOBJS = $(addprefix $(OBJLOC)/, $(addsuffix .o,$(basename $(RESOURCE_FILES))))

## User Command Actions

# "all" makes the pktlog binary
all: application

# "clean" cleans up the compilation directories
clean:
	rm -fv ./$(APPBIN) ./$(OBJLOC)/*.o

# "help" shows assistance for make
help:
	@echo -e "\n-- Make Assistance --\n\nmake help -- Shows help menu\nmake [all] -- Makes the packetlog binary\nmake clean -- Will cleanup all compiled objects\nmake install -- Will compile packetlog and install\nmake uninstall -- Will removed installed packetlog\n"

## Internal Actions

# This is the chain to create our application
application: archbit pktlog

# Needed bit detection for objcopy, This ensures the scripts/archbit.sh script is in place
archbit: /bin/sh scripts/archbit.sh
	$(eval ARCHBIT=$(shell /bin/sh scripts/archbit.sh))

## File Actions

# Make Pktlog :)
$(APPBIN): $(_SRCOBJS) $(_RESOBJS)
	$(CC) $(CFLAGS) $(LINK_FLAGS) -o $@ $^

# Override for pcap.c as it can't tolerate c99 :(
$(OBJLOC)/pktlog_pcap.o: src/pktlog_pcap.c
	$(CC) $(CFLAGS) -c -o $@ $<

# Compile our source into objects for linking :)
$(OBJLOC)/%.o: src/%.c
	$(CC) --std=c99 $(CFLAGS) -c -o $@ $<

# Create our resource objects
$(OBJLOC)/%.o: $(_RESFILES)
	objcopy --input binary --output $(RESOURCEOBJ_TYPE_$(ARCHBIT)) --binary-architecture i386 $^ $@

# Stop from attempting to make a literal file called clean
.PHONY: clean
