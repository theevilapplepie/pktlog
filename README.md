# pktlog
#### Version v.001 - (C) James Vess

## About
PacketLog is a utility similar to NTOP used to gather network information and statistics in order to provide you a good idea of what's going on, on your network.

## Required Software
pcap-devel
sqlite-devel

## Additional Documentation
You can find additional documentation [here](/repositories/pktlog/docs/source/)

## Compiling
### To compile packetlog
[user@box /path/to/source]# make
### To cleanup compilation
[user@box /path/to/source]# make clean
### For help instructions
[user@box /path/to/source]# make help