#!/bin/sh

# Determine System Architecture -- pktlog
# (C) James Vess

if [ -n "`gcc -dumpmachine | grep x86_64`" ]; then
  echo "64"
else
  echo "32"
fi
