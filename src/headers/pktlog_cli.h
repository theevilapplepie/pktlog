/*
  CLI Functions - Header
  (C) James Vess - 2014
*/

int printHelp(char *filename, char *eth);
int parseCliOptions(int *argc, char *argv[], int *verbose_flag, char *addr, int *port, char *eth);
