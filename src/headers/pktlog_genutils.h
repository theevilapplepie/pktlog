/*
  Generic Functions - Header
  (C) James Vess - 2014
*/

void strtolower(char *str);
int changeProcessName(char *name,char *argvz);
int createProcess(char *name, int *callback, char *argvz);
int sendfd(int *socket, int *fd);
void setSigHandler_DefaultCleanup();
int setSigHandler(void *functionhandle);
void pktlog_msleep(int sleepmills);
