/*
 Web Server Functions - Header
 (C) James Vess - 2014
 */

int http_listen(int *sockfd, char *addr, int port);
void http_listen_printerrstr(int errnum);
int http_send(int *newsocketfd, int http_code, char *headercont, char *data );
int http_sendRedirect(int *newsocketfd, char *newuri);
int http_sendCompiledResource(int *newsocketfd, char *type, char *resource_start, char *resource_end);
int http_sendFileSystemResource(int *newsocketfd, char *path);
int http_parseHeader(int *newsocketfd, char *header);
int http_StartService(int *listensock, int max_children, int max_threads, char *argvz);
int http_router(int *newsocketfd, char *path, char *query );
