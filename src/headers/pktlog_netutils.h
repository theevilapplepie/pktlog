/*
  Generic Network Functions - Header
  (C) James Vess - 2014
*/

int isportvalid(int port);
int isportcharvalid(char *charport);
int isipcharvalid(char *ipAddress);
