/*
  Generic PCAP Functions - Header
  (C) James Vess - 2014
*/

int pcap_geteth(char *eth);
int pcap_printdevs();
int pcap_ethexists(char *eth);
