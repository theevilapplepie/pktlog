/*
  Packet Log v.001
  (C) James Vess - 2014
*/

// Include library headers
#include <features.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>

// Include our headers
#include "pktlog_genutils.h"
//#include "pktlog_netutils.h"
#include "pktlog_http.h"
#include "pktlog_pcap.h"
#include "pktlog_cli.h"

int main(int argc, char *argv[]) {

  // Configuration Settings
  int http_max_children = 5; // How many HTTP Workers will we start
  int http_max_threads = 20; // How many HTTP Threads will each worker have
  int mainloop_poll_interval = 10000; // How often in microseconds to wake up and check our processes ( Default: 10000 == 10 milliseconds )
  
  // These are our CLI option/configuration defaults
  int verbose_flag = 0; // How verbose will we be? ( 0 - Standard, 1 - Verbose, 2 - Debug )
  int http_port = 80; // HTTP Server Port
  char http_addr[16] = "0.0.0.0"; // HTTP Server Listen Address

  // System Var Initialization
  char monitor_eth[16] = "";
  int http_socket = 0;
  int funcresult;
  /*
      Process Children
      1 - Database Thread
      2 - Listening Socket
      3 - Web Service
  */
  int process_children[3] = { 0 };

  // Setup our signal handler
  setSigHandler_DefaultCleanup();
  
  // Set the default interface value from system
  pcap_geteth(monitor_eth);

  // Print the Program Header
  printf("\n-- pktlog v.01 --\n\n");
  
  // Parse the command line arguments ( Will not return on failure )
  parseCliOptions(&argc, argv, &verbose_flag, http_addr, &http_port, monitor_eth);
  
  // Print current configuration
  printf("Current Configuration:\n\n\tListen: %s:%i\n\tMonitoring Device: %s\n\tVerbosity: %i\n\n",http_addr,http_port,monitor_eth,verbose_flag);

  // Setup Handler HTTP Listener and validate port / addr availability
  funcresult = http_listen(&http_socket, http_addr, http_port);
  if ( funcresult != 0 ) {
    printf("[!] Could not bind HTTP Listener - ");
    http_listen_printerrstr(funcresult);
    printf(".\n\n");
    printHelp(argv[0],monitor_eth);
  }

  // Work Loop
  for(;;) {
    // Connect/Initialize Database Connections
  
    // Create/Validate HTTP Listening Socket

    // Create Monitoring Process
  
    // Start HTTP Service
    if ( process_children[2] == 0 ) {
      funcresult = http_StartService(&http_socket, http_max_children, http_max_threads, argv[0]);
      if ( funcresult < 0 ) {
        printf("Failed to start HTTP Services, Error: %i", funcresult);
        // Close Application
      }
      process_children[2] = funcresult;
    }
    // Wait for X
    pktlog_msleep(mainloop_poll_interval);
  }
  
  // Show your parent process how much you care.
  return(0);
}
