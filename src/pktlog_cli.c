/*
  CLI Functions
  (C) James Vess - 2014
*/

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <libgen.h>

#include "pktlog_pcap.h"
#include "pktlog_genutils.h"
#include "pktlog_netutils.h"

int printHelp(char *filename, char *eth) {
  
  fprintf(stderr,"Usage Information: %s [arguments]\n\nArguments:\n", basename(filename));
  fprintf(stderr,"    -i/--interface (<device>|list) - Ethernet Device to Monitor ( Default: %s ) or\n"
          "                                     \"list\" to list available devices.\n", eth);
  fprintf(stderr,"    -p/--port (<port>|any) - Port to listen on ( Default: 80 )\n");
  fprintf(stderr,"    -b/--bindaddr <port> - Bind to address ( Default: 0.0.0.0 )\n");
  fprintf(stderr,"    --verbose - Increase Verbosity\n");
  fprintf(stderr,"    --debug - Increase Verbosity Further\n\n");
  
  exit(1);
}

int parseCliOptions(int *argc, char *argv[], int *verbose_flag, char *addr, int *port, char *eth) {
  
  /*
   
    Required Options:
   
    argc - argc from main
    argv - argv from main
    verbose_flag - How verbose will we be? ( 0 - Standard, 1 - Verbose, 2 - Debug )
    addr - The HTTP Address that the client has selected
    port - The HTTP Port that the client has selected
    eth - The monitoring interface the client has selected
   
   */
  
  const struct option long_options[] = {
    /* These options set a flag. */
    {"verbose", no_argument, verbose_flag, 1},
    {"debug", no_argument, verbose_flag, 2},
    /* These options don't set a flag, we distinguish them by their indices */
    {"help", no_argument, 0, 'h'},
    {"interface", required_argument, 0, 'i'},
    {"port",  required_argument, 0, 'p'},
    {0,0,0,0}
  };

  // Lets parse the provided options
  int cmdarg;
  int option_index;
  while ( (cmdarg = getopt_long(*argc, argv, "i:p:b:", long_options, &option_index)) != -1 ) {
    switch (cmdarg) {
        // Catch non-flag arguments
      case 0:
        //printHelp(argv[0]);
        break;
        // Catch help
      case 'h':
        printHelp(argv[0],eth);
        break;
        // Catch Interface
      case 'i':
        // Lowercase Arg
        strtolower(optarg);
        // Determine if new interface is valid or not
        if ( strcmp(optarg, "list") == 0 ) {
          pcap_printdevs();
          exit(1);
        }
        // We don't have a list, We have a dev, lets see if it exists.
        if ( pcap_ethexists(optarg) == 1 ) {
          printf("The requested ethernet device \"%s\" does not exist.\n", optarg);
          pcap_printdevs();
          exit(1);
        }
        // We're good, Save it :)
        strcpy(eth,optarg);
        break;
        // Catch Port
      case 'p':
        // Determine if new port is valid or not
        *port = isportcharvalid(optarg);
        if ( *port == 0 ) {
          printf("[!] The requested port \"%s\" is invalid or outside of the 1-65535 range. Please try again with a different port.\n\n", optarg);
          printHelp(argv[0],eth);
          exit(1);
        }
        break;
        // Catch BindAddr
      case 'b':
        // Lowercase Arg
        strtolower(optarg);
        if (strcmp(optarg,"any") == 0) {
          optarg = "0.0.0.0";
        } else {
          // Determine if new bindaddr is valid or not
          if ( isipcharvalid(optarg) != 1 ) { // SUCCESS ON 1 -- TAKE NOTE!
            printf("[!] The requested bind address \"%s\" is invalid or not bound to this server. Please try again with a different bind address.\n\n",optarg);
            printHelp(argv[0],eth);
          }
        }
        // We're Good, save it :)
        strcpy(addr,optarg);
        break;
        // Catch incorrect arguments
      default:
        printHelp(argv[0],eth);
        break;
    }
  }
  return(0);
}