/*
  Generic Functions
  (C) James Vess - 2014
*/

#define _POSIX_C_SOURCE 199309L

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/socket.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>

// This was stolen then "loosely patched" due to an increment issue :| This is horrible. I feel bad.
void strtolower(char *str) {
  char *pstr = str;
  *pstr = (char)tolower(*pstr);
  while (*pstr++) {
    *pstr = (char)tolower(*pstr);
  }
}

// Shortcuts are pretty neat!
void changeProcessName(char *name,char *argvz) {

  // So, This thing pretty much has to be called from main or we start doing hackish global variables for argv
  // Example: changeProcessName("THIS SHIT IS BANANNAS",argv[0]);
  
  // Clear the memory for argv0 with Null Bytes
  strncpy(argvz,"\0",strlen(argvz));
  // Asisgn the new name ;)
  sprintf(argvz,"%s",name);
  
}

/*int createProcess(char *name, int *callback, char *argvz) {
  
  // Setup some vars
  pid_t childPID;
  
  // Fork our process
  childPID = fork();

  // We're the parent, Return PID or Status
  if ( childPID > 0 ) {
    return((int)childPID);
  } else if ( childPID < 0 ) {
    return(1);
  }
  
  // We're the child, MmmMmm.
  // Lets set our name
  changeProcessName(name,argvz);
  
  // Now lets jump where we were directed
  (*callback)();
  
  // We're done, Exit beautifully ;)
  exit(0);
  
} */

int sendfd(int *socket, int *fd) {

  struct msghdr socketmessage = {0};   // Create Socket Message Header and set all values to 0
  struct cmsghdr *controlmessage;  // Create Control Message Header pointer
  char controlmessagedatafill[CMSG_SPACE(sizeof(int))];   // Determine how much space will be necessary for the control (ancillary) message within the socket message, because we're doing a single file descriptor this would be sizeof int
  int *controlmessagedata;

  /* Remember, "control messages" are also called "Ancillary Data" */
  
  // Setup our message header
  socketmessage.msg_control = controlmessagedatafill; // Put our created storage space into our message header
  socketmessage.msg_controllen = sizeof controlmessagedatafill; // I'm not sure why this is being set here because it gets overwritten later, I'd assume so that CMSG_LEN works.
  
  // Set the pointer for the control message header within the socket message header so we can use it directly
  controlmessage = CMSG_FIRSTHDR(&socketmessage);
  
  // Setup our Control message header
  controlmessage->cmsg_level = SOL_SOCKET; // Means options are to be accesed at socket level, not protocol level
  controlmessage->cmsg_type = SCM_RIGHTS; // Indicate the data array contains the access rights to be sent or received
  controlmessage->cmsg_len = CMSG_LEN(socketmessage.msg_controllen); // Supplies control message byte count including the header, supplied arugment should be size of data not including header
  
  // Copy content into our control data
  controlmessagedata = (int *) CMSG_DATA(controlmessage); // Create a pointer to our data within our control mesasge
  memcpy(controlmessagedata,fd,sizeof(int)); // Copy our file descriptor into our control message data

  // Send data and provide return value
  return sendmsg(*socket, &socketmessage, 0);
}

int recvfd(int *socket) {
  struct msghdr socketmessage = {0};   // Create Socket Message Header and set all values to 0
  struct cmsghdr *controlmessage;  // Create Control Message Header pointer
  char controlmessagedatafill[CMSG_SPACE(sizeof(int))];   // Determine how much space will be necessary for the control (ancillary) message within the socket message, because we're doing a single file descriptor this would be sizeof int
  int *controlmessagedata;
  
  /* Remember, "control messages" are also called "Ancillary Data" */
  
  // Setup our message header
  socketmessage.msg_control = controlmessagedatafill; // Put our created storage space into our message header
  socketmessage.msg_controllen = sizeof controlmessagedatafill; // I'm not sure why this is being set here because it gets overwritten later, I'd assume so that CMSG_LEN works.
  
  if ( recvmsg(*socket, &socketmessage, MSG_CMSG_CLOEXEC) < 0 ) {
    return(-1);
  }
  
  if((socketmessage.msg_flags & MSG_CTRUNC) == MSG_CTRUNC) {
    return(-2);
  }

  for(controlmessage = CMSG_FIRSTHDR(&socketmessage); controlmessage != NULL; controlmessage = CMSG_NXTHDR(&socketmessage, controlmessage)) {
    if ( (controlmessage->cmsg_level == SOL_SOCKET) && (controlmessage->cmsg_type == SCM_RIGHTS) ) {
      controlmessagedata = (int *)CMSG_DATA(controlmessage);
      return *controlmessagedata;
    }
  }
  
  return(-1);
}

void setSigHandler_DefaultCleanup() {
  signal(SIGCHLD, SIG_IGN);
}

int setSigHandler(void *functionhandle) {
  signal(SIGCHLD, SIG_IGN);
  return(1);
}

void pktlog_msleep(int sleepmills) {

  // 1000000000ns = 1s
  // 1000000ns = 1ms
  // 1000ns = 1uf
  
  struct timespec stime, srtime;
  stime.tv_sec  = 0; // Anything higher than a whole second goes here
  stime.tv_nsec = 10000000L; // Anything less than a whole second goes here
  nanosleep(&stime,&srtime);
}