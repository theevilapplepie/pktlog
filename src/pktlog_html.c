/*
  HTML Functions
  (C) James Vess - 2014
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "pktlog_http.h"

int htmlpage_Default(int *newsocketfd) {
  char buffer[1024] = "";
  sprintf(buffer, "<html><body>This is the main page! index.html-ish like page! omg!</body></html>");
  http_send(newsocketfd,200,"",buffer);
  return(0);
}

int htmlpage_404(int *newsocketfd) {
  char buffer[1024] = "";
  sprintf(buffer, "<html><body><h2>404</h2><h4>Page Not found!</h4></body></html>");
  http_send(newsocketfd,200,"",buffer);
  return(0);
}