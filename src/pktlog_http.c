/*
  Web Server Functions
  (C) James Vess - 2014
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <time.h>

#include "pktlog_genutils.h"
#include "pktlog_resources.h"
#include "pktlog_html.h"

// TESTING
#include <sys/stat.h>

void http_log(char *logdata) {
  printf("[date] %s\n", logdata);
}

int http_listen(int *sockfd, char *addr, int port) {

  /*
      Return Codes:
        -1 General Failure
        0 Success
        // Socket Failures
        1 Too many file descriptors open for process
        2 Too many file descriptors open for system
        3 Invalid Access
        4 Not enough buffer space
        // Bind Failures
        5 Address Invalid
        6 Already in Use
        7 Denied - Requires Elevated Permissions
        // Listen Failures
        8 Another sticket is already listening on the same port
  */

  // Init Vars
  struct sockaddr_in srv_addr;  
  
  // Fill srv_addr with Zeros
  bzero((char *) &srv_addr, sizeof(srv_addr));

  // Attempt to create socket
  *sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if ( sockfd < 0 ) {
    switch (errno) {
      case EMFILE:
        return(1);
        break;
      case ENFILE:
        return(2);
        break;
      case EACCES:
        return(3);
        break;
      case ENOBUFS:
        return(4);
        break;
      case EADDRNOTAVAIL:
        return(5);
        break;
      case EADDRINUSE:
        return(6);
        break;  
      default:
        return(-1);
        break;
    }
  }

  // Set the address family for the bind
  srv_addr.sin_family = AF_INET;

  // Bind to Address
  if ( strcmp(addr,"0.0.0.0") == 0 ) {
    srv_addr.sin_addr.s_addr = INADDR_ANY;
  } else {
    srv_addr.sin_addr.s_addr = inet_addr(addr);
  }

  // Convert port number from host byte order to network byte order
  srv_addr.sin_port = htons(port);
  
  // Attempt to Bind
  if (bind(*sockfd, (struct sockaddr *) &srv_addr, sizeof(srv_addr)) < 0) {
    switch(errno) {
      case EADDRNOTAVAIL:
        return(5);
        break;
      case EADDRINUSE:
        return(6);
        break;
      case EACCES:
        return(7);
        break;
      default:
        return(-1);
        break;
    }
  }

  if ( listen(*sockfd,5) < 0 ) {
    switch(errno) {
      case EADDRINUSE:
        return(8);
        break;
      default:
        return(-1);
        break;
    }
  }
  
  // We're GOOD TO GO :)
  return 0;

}

void http_listen_printerrstr(int errnum) {
  switch(errnum) {
    case 0:
      printf("Success");
      break;
    case -1:
      printf("General Failure");
      break;
    case 1:
      printf("Too many file descriptors open for process");
      break;
    case 2:
      printf("Too many file descriptors open for system");
      break;
    case 3:
      printf("Invalid Access");
      break;
    case 4:
      printf("Could not allocate buffers");
      break;
    case 5:
      printf("Address Invalid");
      break;
    case 6:
      printf("Port Already in Use");
      break;
    case 7:
      printf("Access Denied");
      break;
    case 8:
      printf("Socket in Use");
      break;
    default:
      printf("Unknown Failure");
  }
}

int http_send(int *newsocketfd, int http_code, char *headercont, char *data ) {
  // Setup Buffer
  char buffer[1024] = "";
  int writestatus;
  
  // Write HTTP Type/Status Response
  switch (http_code) {
    case 200:
      sprintf(buffer, "HTTP/1.1 200 OK\r\n");
      break;
    case 302:
      sprintf(buffer, "HTTP/1.1 302 FOUND\r\n");
      break;
    case 400:
      sprintf(buffer, "HTTP/1.1 400 BAD REQUEST\r\n");
      break;
    case 401:
      sprintf(buffer, "HTTP/1.1 401 UNAUTHORIZED\r\n");
      break;
    case 403:
      sprintf(buffer, "HTTP/1.1 403 FORBIDDEN\r\n");
      break;
    case 404:
      sprintf(buffer, "HTTP/1.1 404 NOT FOUND\r\n");
      break;
    case 500:
      sprintf(buffer, "HTTP/1.1 500 INTERNAL SERVER ERROR\r\n");
      break;
    case 501:
      sprintf(buffer, "HTTP/1.1 501 NOT IMPLEMENTED\r\n");
      break;
    default:
      return(-1);
  }
  
  writestatus = write(*newsocketfd,buffer,strlen(buffer));
  if ( writestatus == -1 ) {
    printf("Failed to write to socket 1!\n");
    return(-1);
  }
  
  // Write Server
  sprintf(buffer, "Server: pktlog HTTP Server -- Build v.001\r\n");
  writestatus = write(*newsocketfd,buffer,strlen(buffer));
  if ( writestatus == -1 ) {
    printf("Failed to write to socket 2!\n");
    return(-1);
  }
  
  // Write Connection Type
  sprintf(buffer, "Connection: close\r\n");
  writestatus = write(*newsocketfd,buffer,strlen(buffer));
  if ( writestatus == -1 ) {
    printf("Failed to write to socket 3!\n");
    return(-1);
  }

  // Check to see if we have a Content-Type
  if ( strstr(headercont,"Content-Type") == NULL ) {
    // We don't, so write out one :)
    sprintf(buffer, "Content-Type: text/html\r\n");
    writestatus = write(*newsocketfd,buffer,strlen(buffer));
    if ( writestatus == -1 ) {
      printf("Failed to write to socket 4!\n");
      return(-1);
    }
  }
  
  // Write Content-Length
  if ( strlen(data) > 0 ) {
    sprintf(buffer, "Content-Length: %i\r\n",(int)strlen(data));
    writestatus = write(*newsocketfd,buffer,strlen(buffer));
    if ( writestatus == -1 ) {
      printf("Failed to write to socket 5!\n");
      return(-1);
    }
  }

  // Write Header Addtl -- Man this sucks because if a coder doesn't do their \r\n then it's just not going to happen, but there's no real reason to waste cycles on this.
  // Clean up Header
  if ( strlen(headercont) > 0 ) {
    writestatus = write(*newsocketfd,headercont,strlen(headercont));
    if ( writestatus == -1 ) {
      printf("Failed to write to socket 6!\n");
      return(-1);
    }
  }
  
  // Separate header from Data
  sprintf(buffer, "\r\n");
  writestatus = write(*newsocketfd,buffer,strlen(buffer));
  if ( writestatus == -1 ) {
    printf("Failed to write to socket 7!\n");
    return(-1);
  }
  
  // Write Data
  if ( strlen(data) > 0 ) {
    writestatus = write(*newsocketfd,data,strlen(data));
    if ( writestatus == -1 ) {
      printf("Failed to write to socket 8!\n");
      return(-1);
    }
  }
  
  // OMG :D
  return(0);
}

int http_sendRedirect(int *newsocketfd, char *newuri) {
  char buffer[1024] = "";
  sprintf(buffer, "Location: %s\r\n", newuri);
  return(http_send(newsocketfd, 302, buffer, ""));
}

int http_sendCompiledResource(int *newsocketfd, char *type, char *resource_start, char *resource_end) {
  
  // Generate Needed Vars
  int resource_size = resource_end - resource_start;
  char buffer[512];

  // Print HTTP Header
  sprintf(buffer, "Content-Type: %s\r\nContent-Length: %d\r\n", type, resource_size);
  http_send(newsocketfd,200,buffer,""); // It doesn't return anyway :)

  // Write Data
  for ( char *rp = resource_start; rp < resource_end; rp++ ) {
    if ( write(*newsocketfd,rp,1) == -1 ) {
      printf("Failed to write to socket 9!\n");
      return(-1);
    }
  }

  // Return victorious :)
  return(0);
}

int http_sendFileSystemResource(int *newsocketfd, char *path) {

  // SO MUCH TESTING GOING ON :) THIS IS NOT MEANT TO BE PRODUCTION
  /*struct stat st;
  stat("resources/computerguy.jpg", &st);*/
  
  char buffer[1024];
  sprintf(buffer, "Content-Type: image/jpeg\r\nContent-Length: %d\r\n", (int)_binary_resources_computerguy_jpg_size);
  http_send(newsocketfd,200,buffer,"");
  printf("Made it past http_send\n");
  
  /*FILE * pFile;
   int c;
   pFile=fopen ("resources/computerguy.jpg","r");
   if (pFile==NULL) perror ("Error opening file");
   else
   {
   do {
   c = fgetc (pFile);
   send(*newsocketfd,(char *)&c,1,0);
   } while (c != EOF);
   fclose (pFile);
   }
   return 0; */
  
  char *p = &_binary_resources_computerguy_jpg_start;
  //p++;
  /*printf("STARTLOC: %i; ENDLOC: %i; FORMALSIZE: %i; INFORMALSIZE: %i\n",&_binary_resources_computerguy_jpg_start, &_binary_resources_computerguy_jpg_end, &_binary_resources_computerguy_jpg_size, \
         &_binary_resources_computerguy_jpg_end - &_binary_resources_computerguy_jpg_start ); */
  while ( p < &_binary_resources_computerguy_jpg_end ) {
    write(*newsocketfd,p,1);
    //printf("SENDING BLOCK %i\n", p);
    p++;
    
  }
  return(0);
}

int http_router(int *newsocketfd, char *path, char *query ) {
  if ( strcmp(path, "/") == 0 ) {
    htmlpage_Default(newsocketfd);
  } else if ( strcmp(path, "/computerguy.jpg") == 0 ) {
    http_sendCompiledResource(newsocketfd,"image/jpg",&_binary_resources_computerguy_jpg_start,&_binary_resources_computerguy_jpg_end);
  } else {
    htmlpage_404(newsocketfd);
  }
  return(0);
}

int http_parseHeader(int *newsocketfd, char *header) {

  char buffer[1024] = ""; // Used for sending

  char req_type[10] = ""; // This will contain the type of request.
  int req_type_len = sizeof(req_type);

  char req_uri[1024] = ""; // This will contain the request url
  int req_uri_len = sizeof(req_uri);

  char req_querystr[2048] = ""; // This will contain the query string
  int req_querystr_len = sizeof(req_querystr);
  
  // Cursor for Header :)
  int cursor = 0; // Where are we at in the header?
  int charpointer = 0; // Current scan ( with base from 0 )
  
  // Getting the initial request type ( Get, Post, Option, etc )
  for (charpointer = 0; charpointer + cursor < strlen(header); charpointer++) {
    int curheaderloc = cursor + charpointer;
    if ( header[curheaderloc] == (char) 0x20 ) { // Check for space
      charpointer++;
      break;
    }
    if ( header[curheaderloc] == (char) 0x0A ) { // Check for \n
      charpointer++;
      break;
    }
    if ( header[curheaderloc] == (char) 0x0d ) { // Check for \r
      charpointer++;
      break;
    }
    if ( charpointer > req_type_len ) {
      break;
    }
    req_type[charpointer] = header[curheaderloc];
  }

  // Add our current run to the header cursor ;)
  cursor = cursor + charpointer;
  
  // If we don't know what the request type is, Throw a 501
  if ( strcasecmp(req_type,"GET") != 0 && strcasecmp(req_type,"POST") != 0 ) {
    // We don't know what kind of request it is, so kicking it back.
    sprintf(buffer, "Request type \"%s\" is not known to this server.", req_type);
    http_send(newsocketfd, 501, "", buffer);
    return(-1);
  }
  
  // Parse remainder of response
  for ( charpointer = 0; charpointer + cursor < strlen(header); charpointer++) {
    int curheaderloc = cursor + charpointer;
    if ( header[curheaderloc] == (char) 0x20 ) { // Check for space
      cursor++;
      break;
    }
    if ( header[curheaderloc] == (char) 0x0A ) { // Check for \n
      cursor++;
      break;
    }
    if ( header[curheaderloc] == (char) 0x0d ) { // Check for \r
      cursor++;
      break;
    }
    if ( header[curheaderloc] == (char) 0x3F ) { // Check for ? ( eg: start of QUERY String )
      break;
    }
    if ( charpointer > req_uri_len ) {
      break;
    }
    req_uri[charpointer] = header[curheaderloc];
  }
  
  // Add our current run to the header cursor ;)
  cursor = cursor + charpointer;
  
  // Parse Query URI
  if ( header[cursor] == (char) 0x3F ) {
    // Incrmement cursor to clear ?
    cursor++;
    // Lets pull in our querystring.
    for ( charpointer = 0; charpointer + cursor < strlen(header); charpointer++) {
      int curheaderloc = cursor + charpointer;
      if ( header[curheaderloc] == (char) 0x20 ) { // Check for space
        cursor++;
        break;
      }
      if ( header[curheaderloc] == (char) 0x0A ) { // Check for \n
        cursor++;
        break;
      }
      if ( header[curheaderloc] == (char) 0x0d ) { // Check for \r
        cursor++;
        break;
      }
      if ( charpointer > req_querystr_len ) {
        break;
      }
      req_querystr[charpointer] = header[curheaderloc];
    }
    
    // Add our current run to the header cursor ;)
    cursor = cursor + charpointer;
  }
  
  // Lets Route :D
  http_router(newsocketfd, req_uri, req_querystr);

  // Return it like it's hoooooooot
  return(0);
}

int http_StartService(int *listensock, int max_children, int max_threads, char *argvz) {
  
  // Lets split off into our own Service
  pid_t servicePID,childPID;
  servicePID = fork();

  // Are we pktlog or http master?
  if ( servicePID < 0 ) {
    // Failed Fork, Return ERR
    return(-1);
  }
  if ( servicePID > 0 ) {
    // We're the parent, Return the new PID
    return((int) servicePID);
  }

  // We're http master :)
  // Okay, You are to NEVER... EVER.. EVER.. RETURN anything past this point, I mean NEVER, You will go to this thread's main and that is VERY VERY BAD!

  // Swap name to pktlog_http[master]
  changeProcessName("pktlog_http[master]", argvz);
  
  for(;;) { // Okay, Lets keep accepting
    
    // Create our requireds
    int newsocketfd;
    struct sockaddr_in client_addr;
    int client_addr_len = sizeof(client_addr);

    // Get a new socket handle
    newsocketfd = accept(*listensock, (struct sockaddr *) &client_addr, (socklen_t *) &client_addr_len);

    // Check status
    if ( newsocketfd < 0 ) {
      printf("Socket Accept Failure! Reason: %s", strerror(errno));
      exit(-1);
    }

    // Fork our child
    childPID = fork();
    if ( childPID < 0 ) {
      printf("Could not fork process! Reason: %i\n", childPID);
      continue;
    }
    
    // We're the parent
    if ( childPID > 0 ) {
      // Close the accepted child socket
      close(newsocketfd);
      // Return to loop
      continue;
    }

    // We're the CHILD
    changeProcessName("pktlog_http[worker]", argvz); // Change Process Name
    close(*listensock); // Close the parent listening socket

    // Log Connection Open
    http_log("Client Connected");
    
    // Initiate receive Buffer
    char buffer[2049];
    
    // Zero receive buffer
    bzero(buffer,2049);
    
    // Read Data
    int readresult = read(newsocketfd,buffer,2048);
    if ( readresult < 0 ) {
      printf("Error Reading from Socket!\n");
    }

    // Lets parse our HTTP Response
    http_parseHeader(&newsocketfd, buffer); // We actually don't care about the response here. :)

    // Log Connection Close
    http_log("Client Disconnected");
    
    // Close the thread, which will close the accepted socket.
    exit(0);
    
  } // End For Loop
  
  // It's not flying, it's falling with style.
  exit(0);
}