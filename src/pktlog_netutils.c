/*
  Generic Network Functions
  (C) James Vess - 2014
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <arpa/inet.h>

int isportvalid(int port) {
  if ( port < 1 || port > 65535 ) {
    return(0);
  }
  return(port);
}

int isportcharvalid(char *charport) {
  int port = atoi(charport);
  return(isportvalid(port));
}

int isipcharvalid(char *ipAddress) {
  struct sockaddr_in sa;
  int result = inet_pton(AF_INET, ipAddress, &(sa.sin_addr));
  return result;
}