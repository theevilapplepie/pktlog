/*
  Generic PCAP Functions
  (C) James Vess - 2014
*/

#include <stdio.h>
#include <string.h>
#include <pcap.h>

int pcap_geteth(char *eth) {

  char *peth = eth;
  
  // So, This is a weird return by variable reference thing that pcap will use ( a really memory pointer )
  char *dev, errbuf[PCAP_ERRBUF_SIZE];

  // Get Default Eth Device
  dev = pcap_lookupdev(errbuf);

  // If we haven't found one, Don't provide one
  if (dev == NULL) {
    strncpy(peth, "", 10);
    return(-1);
  }

  // Set the output var to our device and return happy
  strncpy(peth, dev, 10);
  return(0);

}

int pcap_printdevs() {

  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_if_t *alldevsp;

  if(pcap_findalldevs(&alldevsp, errbuf) == -1) {
    fprintf(stderr,"** pcap_listdevs: Error listing devices \"%s\"\n",errbuf);
    void pcap_freealldevs(pcap_if_t *alldevsp);
    return -1;
  }

  printf("\nAvailable Devices:\n\n");

  while ((alldevsp)!=NULL) {

    if ( (alldevsp)->flags & PCAP_IF_LOOPBACK ) {
      (alldevsp)=(alldevsp)->next;
      continue;
    }

    // Print the Interface
    printf("    %s",(alldevsp)->name);

    // Give Desc
    if ( (alldevsp)->description != NULL ) {
      printf(" - %s", (alldevsp)->description);
    }

    // Newline me
    printf("\n");

    // NEXT!
    (alldevsp)=(alldevsp)->next;
  }

  printf("\n");

  void pcap_freealldevs(pcap_if_t *alldevsp);
  return 0;
}

int pcap_ethexists(char *eth) {

  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_if_t *alldevsp;

  if(pcap_findalldevs(&alldevsp, errbuf) == -1) {
    fprintf(stderr,"** pcap_ethexists: Error listing devices \"%s\"\n",errbuf);
    void pcap_freealldevs(pcap_if_t *alldevsp);
    return -1;
  }

  // Iterate and lets see if we have a matching eth
  while ((alldevsp)!=NULL) {
    // If we have a match, return it.
    if ( strcmp(eth, (alldevsp)->name) == 0 ) {
      void pcap_freealldevs(pcap_if_t *alldevsp);
      return 0;
    }
    // NEXT!
    (alldevsp)=(alldevsp)->next;
  }

  void pcap_freealldevs(pcap_if_t *alldevsp);
  return 1;
}
